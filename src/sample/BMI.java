package sample;

/**
 * Created by lukasz on 08.06.17.
 */
public class BMI {

    private short height;
    private short weight;

    public void setAll(short height, short weight) {
        this.height = height;
        this.weight = weight;
    }

    private double cmToMeter(int cm) {
        return (double) cm / 100;
    }

    public double round() {
        return Math.round(getMBI() * 100.0) / 100.0;
    }

    public double getMBI() {
        if (cmToMeter(height) != 0)
            return weight / (cmToMeter(height) * cmToMeter(height));
        else
            return 0.0;
    }

    public void clearAll() {
        height = 0;
        weight = 0;
    }
}
