package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import static sample.Menu.*;


public class Controller {

    @FXML public Text resultText;
    @FXML public TextField heightTxtField;
    @FXML public TextField weightTxtField;
    private BMI bmi = new BMI();

    @FXML public void calculateBMI(ActionEvent actionEvent) {
        getValueFromUser();
    }

    @FXML public void clearValue(ActionEvent actionEvent) {
        clearText();
    }

    private void getValueFromUser() {

        if (!heightTxtField.getText().isEmpty() && !weightTxtField.getText().isEmpty()) {
            try {
                bmi.setAll(Short.parseShort(heightTxtField.getText()), Short.parseShort(weightTxtField.getText()));
                setText(bmi);
            } catch (NumberFormatException e) {
                clearText();
                setText(WRONG_DATA);
            }
        } else
            setText(FILL_ALL_FIELDS);
    }

    private void setText(BMI bmiObj) {
        resultText.wrappingWidthProperty().set(300);

        if (bmiObj.getMBI() != 0.0)
            resultText.setText(showBmiInfo(bmi));
        else
            setText(WRONG_DATA);
    }

    private void setText(String text) {
        resultText.wrappingWidthProperty().set(300);
        resultText.setText(text);
    }

    private void clearText() {
        resultText.setText("");
        heightTxtField.clear();
        weightTxtField.clear();
        bmi.clearAll();
    }
}
