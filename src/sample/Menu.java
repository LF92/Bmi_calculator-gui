package sample;

/**
 * Created by lukasz on 07.06.17.
 */
public class Menu {
    static final String WRONG_DATA = "Wprowadzono nieprawidłowe dane,spróbuj ponownie";
    static final String FILL_ALL_FIELDS = "Wypełnij oba pola";

    static String showBmiInfo(BMI bmiObj) {
        double bmii = bmiObj.round();
        return "Twoje BMI wynosi: " + bmii + ", oznacza to " + BMI_Info(bmii);
    }

    private static String BMI_Info(double bmi) {
        if (bmi < 16.00)
            return "wygłoszenie";
        else if (bmi >= 16.00 && bmi < 17.00)
            return "wychudzenie";
        else if (bmi >= 17.00 && bmi < 18.50)
            return "niedowagę";
        else if (bmi >= 18.50 && bmi < 25.00)
            return "wartość prawidłową";
        else if (bmi >= 25.00 && bmi < 30.00)
            return "nadwagę";
        else if (bmi >= 30.00 && bmi < 35.00)
            return "I stopień otyłości";
        else if (bmi >= 35.00 && bmi < 40.00)
            return "II stopień otyłości (otyłość kliniczna)";
        else if (bmi >= 40.00)
            return "III stopień otyłości (otyłość skrajna)";
        else
            return "Błąd";
    }
}
